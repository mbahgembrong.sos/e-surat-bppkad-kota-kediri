@extends('admin.layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Arsip Masuk</h1>
    {{-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
        For more information about DataTables, please visit the <a target="_blank"
            href="https://datatables.net">official DataTables documentation</a>.</p> --}}
    <p class="text-muted m-b-30 font-14">Berikut adalah data seluruh Arsip Masuk</p>
    @if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
    @endif

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-md-7">
                    <h6 class="m-0 font-weight-bold text-primary">Laporan Arsip Masuk</h6>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="get">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Awal</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="date" placeholder="Masukkan tanggal awal"
                                    name='tanggal_awal' id="tanggal_awal">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Status</label>
                            <div class="col-sm-7">
                                <select class="js-example-basic-single form-control" name="status">
                                    <option value="Semua" selected>Semua</option>
                                    <option value="Arsipkan">Arsip</option>
                                    <option value="Tindak Lanjuti">Tindak Lanjuti</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary btn-icon-split btn-sm float-right">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-search"></i>
                                    </span>
                                    <span class="text">Tampilkan</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Selesai</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="date" placeholder="Masukkan tanggal selesai"
                                    name='tanggal_selesai' id="tanggal_selesai">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Cetak</label>
                            <div class="col-sm-2">
                                <a href="{{ route('laporan.arsipmasuk.pdf') }}" class="btn btn-warning btn-icon-split btn-sm float-right">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-print"></i>
                                    </span>
                                    <span class="text">Print</span>
                                </a>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{ route('laporan.arsipmasuk.excel') }}"
                                    class="btn btn-success btn-icon-split btn-sm float-right">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-file-excel"></i>
                                    </span>
                                    <span class="text">Excel</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Pengirim</th>
                            <th scope="col">Tanggal Diterima</th>
                            <th scope="col">Tanggal Surat</th>
                            <th scope="col">Perihal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php ($no=0) @endphp
                        @foreach($arsipMasuk as $data)
                        <tr>
                            <td>{{$no+=1}}</td>
                            <td>{{$data->pengirim}}</td>
                            <td>{{ date_format(date_create($data->tgl_terima),"d/m/Y") }}</td>
                            <td>{{ date_format(date_create($data->tgl_surat),"d/m/Y")}}</td>
                            <td>{{$data->perihal}}</td>
                        </tr>
                        @endforeach
                        @forelse($arsipMasuk as $data)
                        @empty
                        <tr class='text-center'>
                            <td colspan="4">Tidak ada data</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection
@section('script')
<script>
    // tanggal_awal= $("input[name=tanggal_awal]").val();
    // tanggal_selesai= $("input[name=tanggal_selesai]").val();
    // status= $("input[name=status]").val();
    // $( "#excel" ).click(function(e) {
    //     e.preventDefault();
    //     $.ajax({
    //     url: "/laporan/arsipmasuk/export_excel",
    //     type: "get", //send it through get method
    //     data: {
    //     tanggal_awal:this.tanggal_awal,
    //     tanggal_selesai:this.tanggal_selesai,
    //     status:this.status,
    //     },
    //     success: function(response) {
    //     console.log(response)
    //     },
    //     error: function(xhr) {
    //     console.log(xhr)

    //     }
    //     });
    // });
</script>
@endsection
