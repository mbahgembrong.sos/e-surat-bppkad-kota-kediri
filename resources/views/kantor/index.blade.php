@extends('admin.layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Kantor</h1>
    {{-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
        For more information about DataTables, please visit the <a target="_blank"
            href="https://datatables.net">official DataTables documentation</a>.</p> --}}
    <p class="text-muted m-b-30 font-14">Berikut adalah data seluruh Kantor</p>
    @if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
    @endif
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-md-7">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar Kantor</h6>
                </div>
                <div class="col-md-5">
                    <a href="{{ route('kantor.create') }}" class="btn btn-primary btn-icon-split btn-sm float-right">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Kantor</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Alamat</th>
                            <th scope="col" style="text-align:center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php ($no=0) @endphp
                        @foreach($kantor as $data)
                        <tr>
                            <td>{{$no+=1}}</td>
                            <td>{{$data->nama}}</td>
                            <td>{{$data->alamat}}</td>
                            <td colspan="2">
                                <div class="d-inline-flex">
                                    <form method="POST" action="{{ route('kantor.destroy',$data->id) }}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ route('kantor.edit',$data->id)}}"
                                            class="btn btn-warning btn-xs text-white"><i
                                                class="fas fa-edit"></i></i></a>
                                        <button class="btn btn-danger btn-xs"
                                            onclick="return confirm('Yakin ingin menghapus data ini ?')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @forelse($kantor as $data)
                        @empty
                        <tr class='text-center'>
                            <td colspan="4">Tidak ada data</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

@endsection
