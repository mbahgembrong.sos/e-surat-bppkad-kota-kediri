<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Pengirim</th>
            <th>Penerima</th>
            <th>Tanggal Surat</th>
            <th>Tanggal Kirim</th>
            <th>Nomor Surat</th>
            <th>Keterangan</th>
            <th>Perihal</th>
            <th>File</th>
        </tr>
    </thead>
    <tbody>
        @php ($no=0) @endphp
        @foreach($arsipkeluars as $arsipkeluar)
        <tr>
            <td>{{$no+=1}}</td>
            <td>{{ $arsipkeluar->pengirim }}</td>
            <td>{{ $arsipkeluar->penerima }}</td>
            <td>{{ $arsipkeluar->tgl_surat->format('d/m/Y') }}</td>
            <td>{{ $arsipkeluar->tgl_kirim->format('d/m/Y') }}</td>
            <td>{{ $arsipkeluar->no_surat }}</td>
            <td>{{ $arsipkeluar->keterangan }}</td>
            <td>{{ $arsipkeluar->perihal }}</td>
            <td>{{ $arsipkeluar->file }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
