<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Pengirim</th>
            <th>Tanggal Surat</th>
            <th>Tanggal Terima</th>
            <th>Nomor Surat</th>
            <th>Sifat</th>
            <th>Klasifikasi</th>
            <th>Keterangan</th>
            <th>Perihal</th>
            <th>Isi</th>
            <th>File</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @php ($no=0) @endphp
        @foreach($arsipmasuks as $arsipmasuk)
        <tr>
            <td>{{$no+=1}}</td>
            <td>{{ $arsipmasuk->pengirim }}</td>
            <td>{{ $arsipmasuk->tgl_surat->format('d/m/Y') }}</td>
            <td>{{ $arsipmasuk->tgl_terima->format('d/m/Y') }}</td>
            <td>{{ $arsipmasuk->no_surat }}</td>
            <td>{{ $arsipmasuk->sifat }}</td>
            <td>{{ $arsipmasuk->klasifikasi }}</td>
            <td>{{ $arsipmasuk->keterangan }}</td>
            <td>{{ $arsipmasuk->perihal }}</td>
            <td>{{ $arsipmasuk->isi }}</td>
            <td>{{ $arsipmasuk->file }}</td>
            <td>{{ $arsipmasuk->status }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
