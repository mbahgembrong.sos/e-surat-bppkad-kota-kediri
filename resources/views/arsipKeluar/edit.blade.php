@extends('admin.layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Ubah Surat Masuk</h6>
        </div>
        <div class="card-body">
            <form action="{{route('arsipkeluar.update',$arsipkeluar->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Pengirim</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" placeholder="Masukkan pengirim" name='pengirim'
                                    id="example-text-input" value="{{ $arsipkeluar->pengirim }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Surat</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="date" placeholder="Masukkan tanggal surat"
                                    name='tanggal_surat' id="example-text-input"
                                    value="{{ $arsipkeluar->tgl_surat->format('Y-m-d') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Nomor Surat</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" placeholder="Masukkan nomor surat"
                                    name='nomor_surat' id="example-text-input" value="{{ $arsipkeluar->no_surat }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Dikirim</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="date" placeholder="Masukkan tanggal dikirim"
                                    name='tanggal_kirim' id="example-text-input"
                                    value="{{ $arsipkeluar->tgl_kirim->format('Y-m-d') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" placeholder="Masukkan keterangan" name='keterangan'
                            id="example-text-input" value="{{ $arsipkeluar->keterangan }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Tujuan</label>
                    <div class="col-sm-10">
                        <select class="js-example-placeholder-single form-control" name="penerima" >
                            @foreach ($kantors as $kantor)
                            <option value="{{ $kantor->nama }}"{{ $arsipkeluar->penerima == $kantor->nama ? 'selected':'' }}>{{ $kantor->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Perihal</label>
                    <div class="col-sm-10">
                        <textarea id="perihal" name="perihal">{{ $arsipkeluar->perihal }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Surat</label>
                    <div class="col-sm-10">
                        {{-- <input type="file" name="surat" /> --}}
                        <input type="file" class="my-pond" id="upload" name="surat" />
                    </div>
                </div>
                <div class="float-right">
                    <a href="{{ route('arsipkeluar.index') }}" class='btn btn-light pl-md-3'>Cancel</a>
                    <button type="submit" class='btn btn-primary'>Submit</button>
                </div>
            </form>
        </div>
    </div>

</div>
<input type="hidden" id="filemasuk" value="{{ $arsipkeluar->file }}">
@endsection
@section('script')
<script>
    var fileMasuk = document.querySelector('input[id="filemasuk"]').value;

    tinymce.init({
        forced_root_block : false,
    selector: 'textarea#perihal'
    });
    tinymce.init({
        forced_root_block : false,
    selector: 'textarea#disposisi'
    });
    $(document).ready(function() {
    $('.js-example-placeholder-single').select2({placeholder: "Pilih penerima"});
    });
    $(function(){

    // Register the plugin
    FilePond.registerPlugin(FilePondPluginFileValidateType);
    const inputElement = document.querySelector('input[id="upload"]');

    // Create a FilePond instance
    const pond = FilePond.create(inputElement, {
        files: [
        {
        // the server file reference
        source: 'storage/masuk'+fileMasuk ,

        // set type to local to indicate an already uploaded file
        options: {
        type: 'local',

        // mock file information
        file: {
        name: fileMasuk,
        },
        },
        },
        ],
        disabled:true,
        storeAsFile: true,
        acceptedFileTypes: ['application/pdf','image/*'],
        fileValidateTypeDetectType: (source, type) =>
        new Promise((resolve, reject) => {
        // Do custom type detection here and return with promise

        resolve(type);
        }),
    });

    });
</script>
@endsection
