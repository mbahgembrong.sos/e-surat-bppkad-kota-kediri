@extends('admin.layouts.app')
@section('content')
<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Detail Surat Masuk</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Pengirim</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" placeholder="Masukkan pengirim" name='pengirim'
                                readonly id="example-text-input" value="{{ $arsipmasuk->pengirim }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Surat</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" placeholder="Masukkan tanggal surat"
                                name='tanggal_surat' readonly id="example-text-input"
                                value="{{ $arsipmasuk->tgl_surat->format('Y-m-d') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Sifat</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" placeholder="Masukkan Sifat" name='Sifat' readonly
                                id="example-text-input" value="{{ $arsipmasuk->sifat }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Keterangan</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" placeholder="Masukkan Keterangan" name='Keterangan'
                                readonly id="example-text-input" value="{{ $arsipmasuk->keterangan }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Nomor Surat</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" placeholder="Masukkan nomor surat" readonly
                                name='nomor_surat' id="example-text-input" value="{{ $arsipmasuk->no_surat }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Diterima</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" placeholder="Masukkan tanggal diterima" readonly
                                name='tanggal_terima' id="example-text-input"
                                value="{{ $arsipmasuk->tgl_terima->format('Y-m-d') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Klasifikasi</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" placeholder="Masukkan Klasifikasi" readonly
                                name='klasifikasi' id="example-text-input" value="{{ $arsipmasuk->klasifikasi }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">status</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" placeholder="Masukkan status" readonly name='status'
                                id="example-text-input" value="{{ $arsipmasuk->status }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <table class="table table-hover table-bordered table-condensed">
                    <thead>
                        <tr>
                            <td>Perihal</td>
                            <td>Diteruskan Kepada</td>
                            <td>Isi Disposisi</td>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            {{ $arsipmasuk->status }}

                        </td>
                        <td>
                            @foreach ($detailmasuk as $detail )
                            <p>{{ $detail->bidang }}</p>
                            @endforeach
                        </td>
                        <td>
                            {{ $arsipmasuk->isi }}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="float-right">
                <a href="{{ route('arsipmasuk.index') }}" class='btn btn-light pl-md-3'>Cancel</a>
                <a href="{{ route('arsipmasuk.edit',$arsipmasuk->id) }}" class='btn btn-primary'>Edit</a>
            </div>
        </div>
    </div>
</div>
<style>
    .pdfobject-container {
        height: 30rem;
        border: 1rem solid rgba(0, 0, 0, .1);
    }
</style>
<input type="hidden" id="filemasuk" value="{{ asset('storage/masuk/'.$arsipmasuk->file) }}">
@endsection
@section('script')
{{-- <script src="/js/pdfobject.js"></script> --}}
{{-- <script>
    var fileMasuk = document.querySelector('input[id="filemasuk"]').value;
    PDFObject.embed(fileMasuk, "#pdfviewer");
</script> --}}
{{--
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <center>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <table class="table text-lg-left">
                    <tr>
                        <th>ID</th>

                    </tr>
                    <tr>
                        <th>Nama role</th>
                        <td>:</td>

                    <tr>
                        <th>Deskripsi role</th>
                        <td>:</td>

                    <tr>
                        <th>Harga sewa / Hari</th>
                        <td>:</td>

                    </tr>
                    <tr>
                        <th>Terdaftar Pada</th>
                        <td>:</td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</div> --}}
@endsection
