<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         User::create([
            'nama'      => 'admin',
            'nama_role'      => 'admin',
            'email'     => 'admin@gmail.com',
            'password'  => Hash::make('admin')
        ]);
    }
}
