<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kantor extends Model
{
    // use HasFactory;
    protected $table = 'kantor';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['nama','alamat'];
}
