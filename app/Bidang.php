<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bidang extends Model
{
    // use HasFactory;
    protected $table = 'bidang';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['nama_role','nama'];
}
