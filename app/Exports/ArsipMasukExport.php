<?php

namespace App\Exports;

use App\ArsipMasuk;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ArsipMasukExport implements FromView
{


//  function __construct($arsipMasuk) {
//         $this->arsipMasuk = $arsipMasuk;
//  }
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return $this->arsipMasuk;
    //     // return ArsipMasuk::whereBetween('tgl_terima', [$from, $to]);
    // }
    public function view(): View
    {
        return view('cetak.masukExcel', [
            'arsipmasuks' => ArsipMasuk::all()
        ]);
    }
}
