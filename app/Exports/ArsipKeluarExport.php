<?php

namespace App\Exports;

use App\ArsipKeluar;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ArsipKeluarExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
     public function view(): View
    {
        return view('cetak.keluarExcel', [
            'arsipkeluars' => ArsipKeluar::all()
        ]);
    }
}
