<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid as Generator;

class DetailMasuk extends Model
{
    // use HasFactory;
    protected $table = 'detail_masuk';

    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = ['id','id_masuk','bidang'];
    public function detail(){
        return $this -> belongsTo(ArsipMasuk::class);
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
