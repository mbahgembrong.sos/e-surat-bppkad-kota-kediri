<?php

namespace App\Http\Controllers;

use App\ArsipMasuk;
use App\Bidang;
use App\DetailMasuk;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use PDF;
use Alert;

class ArsipMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arsipMasuk = ArsipMasuk::get();
        return view('arsipMasuk.index', compact('arsipMasuk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bidangs = Bidang::all();
        return view('arsipMasuk.add' ,compact(['bidangs']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                'pengirim' => 'required',
                'tanggal_surat' => 'required',
                'nomor_surat' => 'required',
                'tanggal_terima' => 'required',
                'perihal' => 'required',
                'surat' => 'required',
            ],
                [
                'pengirim.required' => 'Pengirim  Harus diisi',
                'tanggal_surat.required' => 'Tanggal Surat  Harus diisi',
                'nomor_surat.required' => 'Nomor Surat  Harus diisi',
                'tanggal_terima.required' => 'Tanggal Terima  Harus diisi',
                'perihal.required' => 'Perihal Harus diisi',
                'surat.required' => 'File Surat Harus diisi',
            ]
            );

            if($request->hasFile('surat')){
                $filenameWithExt = $request->file('surat')->getClientOriginalName();
            //Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('surat')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('surat')->storeAs('public/masuk',$fileNameToStore);
            }

            $arsipMasuk = new ArsipMasuk;
            $arsipMasuk->pengirim = $request->pengirim;
            $arsipMasuk->tgl_surat = $request->tanggal_surat;
            $arsipMasuk->no_surat = $request->nomor_surat;
            $arsipMasuk->tgl_terima = $request->tanggal_terima;
            if (isset($request->keterangan)) {
                $arsipMasuk->keterangan = $request->keterangan;
            }
            if (isset($request->keterangan)) {
                $arsipMasuk->keterangan = $request->keterangan;
            }
            if (isset($request->sifat)) {
                $arsipMasuk->sifat = $request->sifat;
            }
            if (isset($request->klasifikasi)) {
                $arsipMasuk->klasifikasi = $request->klasifikasi;
            }
            if (isset($request->disposisi)) {
                $arsipMasuk->isi = $request->disposisi;
            }
            if (isset($request->status)) {
                $arsipMasuk->status = $request->status;
            }
            $arsipMasuk->perihal = $request->perihal;
            $arsipMasuk->file = $fileNameToStore;
            if (Auth::user()->nama_role == "admin") {
                $arsipMasuk->bidang = "all" ;
            } else {
                $arsipMasuk->bidang =  Auth::user()->nama_role;
            }
            $arsipMasuk->save();

            foreach ($request->kepada as $bidang) {
                $arsipMasukDetail = new DetailMasuk;
                $arsipMasukDetail->bidang = $bidang;
                $arsipMasukDetail->id_masuk = $arsipMasuk->id;
                $arsipMasukDetail->save();
            }
            Alert::success('Tambah Arsip Masuk', 'Data berhasil disimpan');
            return redirect()->route('arsipmasuk.index');
        }catch(\Exception $e)
            {
                Alert::warning('Tambah Arsip Masuk', 'Gagal Tambah Data.');
                return redirect()->back();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArsipMasuk  $arsipMasuk
     * @return \Illuminate\Http\Response
     */
    public function show( $arsipMasuk)
    {
        $arsipmasuk = ArsipMasuk::findOrfail($arsipMasuk);
        $detailmasuk = DetailMasuk::where('id_masuk', '=', $arsipmasuk->id)->get();
        $bidangs = Bidang::all();
        return view('arsipMasuk.show', compact(['arsipmasuk','detailmasuk','bidangs']));
    }
    public function cetakPDF( $arsipMasuk)
    {
        $arsipmasuk = ArsipMasuk::findOrfail($arsipMasuk);
        $detailmasuk = DetailMasuk::where('id_masuk', '=', $arsipmasuk->id)->get();
        return view('cetak.masukPDF', compact('arsipmasuk','detailmasuk'));
        // $pdf = PDF::loadview('cetak.masukPDF',['arsipmasuk'=>$arsipmasuk,'detailmasuk'=>$detailmasuk]);
    	// return $pdf->download('lembar-disposisi-arsipmasuk.pdf');
    }
    public function download( $arsipMasuk)
    {
        $arsipmasuk = ArsipMasuk::findOrfail($arsipMasuk);
        // return "storage/masuk/$arsipmasuk->file";
        $file= public_path(). "/storage/masuk/$arsipmasuk->file";
        return response()->download($file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArsipMasuk  $arsipMasuk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arsipmasuk = ArsipMasuk::findOrfail($id);
        $detailmasuk = DetailMasuk::where('id_masuk', '=', $arsipmasuk->id)->get();
        $bidangs = Bidang::all();
        // dd($detailmasuk);
        return view('arsipMasuk.edit', compact(['arsipmasuk','detailmasuk','bidangs']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArsipMasuk  $arsipMasuk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
             $request->validate(
                [
                'pengirim' => 'required',
                'tanggal_surat' => 'required',
                'nomor_surat' => 'required',
                'tanggal_terima' => 'required',
                'perihal' => 'required',
            ],
                [
                'pengirim.required' => 'Pengirim  Harus diisi',
                'tanggal_surat.required' => 'Tanggal Surat  Harus diisi',
                'nomor_surat.required' => 'Nomor Surat  Harus diisi',
                'tanggal_terima.required' => 'Tanggal Terima  Harus diisi',
                'perihal.required' => 'Perihal Harus diisi',
            ]
            );

            $arsipMasuk = ArsipMasuk::findOrfail($id);
            $arsipMasuk->pengirim = $request->pengirim;
            $arsipMasuk->tgl_surat = $request->tanggal_surat;
            $arsipMasuk->no_surat = $request->nomor_surat;
            $arsipMasuk->tgl_terima = $request->tanggal_terima;
            if (isset($request->keterangan)) {
                $arsipMasuk->keterangan = $request->keterangan;
            }
            if (isset($request->keterangan)) {
                $arsipMasuk->keterangan = $request->keterangan;
            }
            if (isset($request->sifat)) {
                $arsipMasuk->sifat = $request->sifat;
            }
            if (isset($request->klasifikasi)) {
                $arsipMasuk->klasifikasi = $request->klasifikasi;
            }
            if (isset($request->disposisi)) {
                $arsipMasuk->isi = $request->disposisi;
            }
            if (isset($request->status)) {
                $arsipMasuk->status = $request->status;
            }
            $arsipMasuk->perihal = $request->perihal;

            if (Auth::user()->nama_role == "admin") {
                $arsipMasuk->bidang = "all" ;
            } else {
                $arsipMasuk->bidang =  Auth::user()->nama_role;
            }
            $arsipMasuk->update();

            foreach ($request->kepada as $bidang) {
                $arsipMasukDetail = new DetailMasuk;
                $arsipMasukDetail->bidang = $bidang;
                $arsipMasukDetail->id_masuk = $arsipMasuk->id;
                $arsipMasukDetail->save();
            }
            Alert::success('Update Arsip Masuk', 'Data berhasil dirubah');
            return redirect()->route('arsipmasuk.index');
        } catch (\Exception $th) {
             Alert::warning('Update Arsip Masuk', 'Gagal Rubah Data.');
                return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArsipMasuk  $arsipMasuk
     * @return \Illuminate\Http\Response
     */
    public function destroy($arsipMasuk)
    {
        try {
            $arsipMasuk = ArsipMasuk::findOrfail($arsipMasuk);
        if(is_file($arsipMasuk->file))
        {
        Storage::delete($arsipMasuk->file);
        Storage::delete('public/masuk/'.$arsipMasuk->file);
        unlink(storage_path("public/masuk/$arsipMasuk->file"));
        }
        $arsipMasuk->delete();
        Alert::success('Delete Arsip Keluar', 'Data berhasil dihapus');
        return redirect()->back();
        } catch (\Exception $th) {
            Alert::warning('Delete Arsip Masuk', 'Gagal Hapus Data.');
                return redirect()->back();
        }

    }
}
