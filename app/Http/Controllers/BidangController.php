<?php

namespace App\Http\Controllers;

use App\Bidang;
use App\Role;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Alert;
class BidangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bidang = Bidang::get();
        return view('bidang.index', compact('bidang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('bidang.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $id = IdGenerator::generate(['table' => 'bidang', 'field'=>'id', 'length' => 10, 'prefix' =>'BDG-']);

        $request->validate(
            [
                'nama' => 'required|min:3',
                'role' => 'required'
            ],
            [
                'nama.required' => 'Nama Bidang Harus diisi',
                'role.required' => 'Nama Role Harus diisi',
                'nama.min' => 'Nama Bidang Minimal 3 Karakter'
            ]
        );

        $bidang = new Bidang;
        $bidang->id = $id;
        $bidang->nama = $request->nama;
        $bidang->nama_role = $request->role;
        $bidang->save();
        Alert::success('Tambah Bidang', 'Data berhasil disimpan');
        return redirect()->route('bidang.index');
        } catch (\Exception $th) {
            Alert::warning('Tambah Bidang', 'Gagal Tambah Data.');
                return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bidang  $bidang
     * @return \Illuminate\Http\Response
     */
    public function show($bidang)
    {
        // var_dump($bidang);
        $bidang = Bidang::findOrfail($bidang);
        return view('bidang.show', compact('bidang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bidang  $bidang
     * @return \Illuminate\Http\Response
     */
    public function edit( $bidang)
    {
        $bidang = Bidang::findOrfail($bidang);
        $roles = Role::all();
        return view('bidang.edit' ,compact(['bidang','roles']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bidang  $bidang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$bidang)
    {
        try {
             $request->validate(
            [
                'nama' => 'required|min:3',
                 'role' => 'required',
            ],
            [
                'nama.required' => 'Nama Bidang Harus diisi',
                'nama.min' => 'Nama Bidang Minimal 3 Karakter',
                'role.required' => 'Nama Role Harus diisi',
            ]
        );

        $bidang = Bidang::findOrFail($bidang);
        $bidang->nama = $request->nama;
        $bidang->nama_role = $request->role;
        $bidang->update();
        Alert::success('Update Bidang', 'Data berhasil dirubah');
        return redirect()->route('bidang.index');
    } catch (\Exception $th) {
        Alert::warning('Update Bidang', 'Gagal Rubah Data.');
        return redirect()->back();
    }
}


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bidang  $bidang
     * @return \Illuminate\Http\Response
     */
    public function destroy($bidang)
    {
        try {
            $bidang = Bidang::findOrFail($bidang);
        $bidang->delete();
         Alert::success('Delete Bidang', 'Data berhasil dihapus');
        return redirect()->back();
        } catch (\Throwable $th) {
             Alert::warning('Delete Kantor', 'Gagal Hapus Data.');
            return redirect()->back();
        }

    }
}
